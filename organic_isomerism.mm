<map version="freeplane 1.6.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node FOLDED="false" ID="ID_1579196460" CREATED="1542476133511" MODIFIED="1542479657600" STYLE="oval"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>organic isomerism</b>
    </p>
  </body>
</html>

</richcontent>
<font SIZE="18"/>
<hook NAME="MapStyle">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="14" RULE="ON_BRANCH_CREATION"/>
<node TEXT="" POSITION="right" ID="ID_1046889547" CREATED="1542479537080" MODIFIED="1542479537092">
<edge COLOR="#ff0000"/>
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT=" 1) &#x418;&#x43d;&#x442;&#x435;&#x440;&#x444;&#x435;&#x439;&#x441;" POSITION="right" ID="ID_386871781" CREATED="1542479106281" MODIFIED="1542479214577" HGAP_QUANTITY="24.499999687075622 pt" VSHIFT_QUANTITY="-59.24999823421245 pt">
<edge COLOR="#ff0000"/>
<font SIZE="24"/>
<cloud COLOR="#f0f0f0" SHAPE="ARC"/>
</node>
<node TEXT="" POSITION="right" ID="ID_402902733" CREATED="1542479537072" MODIFIED="1542479746085">
<edge COLOR="#7c7c00"/>
<hook NAME="SummaryNode"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="1.1)&#x41f;&#x440;&#x43e;&#x441;&#x442; &#x432; &#x438;&#x441;&#x43f;&#x43e;&#x43b;&#x44c;&#x437;&#x43e;&#x432;&#x430;&#x43d;&#x438;&#x438;" ID="ID_720016198" CREATED="1542479537096" MODIFIED="1542479746080" HGAP_QUANTITY="37.999999284744284 pt" VSHIFT_QUANTITY="-83.24999751895673 pt">
<font SIZE="30"/>
</node>
<node TEXT="" ID="ID_514548739" CREATED="1542479580359" MODIFIED="1542479747925">
<node TEXT="1.2)&#x423;&#x434;&#x43e;&#x431;&#x43d;&#x44b;&#x439; &#x438; &#x43f;&#x440;&#x438;&#x44f;&#x442;&#x43d;&#x44b;&#x439;  &#x434;&#x43b;&#x44f; &#x443;&#x447;&#x435;&#x43d;&#x438;&#x43a;&#x43e;&#x432;" ID="ID_1270353871" CREATED="1542479602513" MODIFIED="1542479747925" HGAP_QUANTITY="78.49999807775026 pt" VSHIFT_QUANTITY="-11.999999642372142 pt">
<font SIZE="20"/>
</node>
</node>
</node>
<node TEXT="" POSITION="right" ID="ID_1351504484" CREATED="1542479673784" MODIFIED="1542479673788">
<edge COLOR="#00ff00"/>
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="2) &#x421;&#x43e;&#x437;&#x434;&#x430;&#x43d;&#x438;&#x435; 3D &#x43c;&#x43e;&#x434;&#x435;&#x43b;&#x435;&#x439; &#x43c;&#x43e;&#x43b;&#x435;&#x43a;&#x443;&#x43b;" POSITION="right" ID="ID_1394599789" CREATED="1542479206898" MODIFIED="1542479671637" HGAP_QUANTITY="39.4999992400408 pt" VSHIFT_QUANTITY="65.99999803304678 pt">
<edge COLOR="#00ff00"/>
<font SIZE="24"/>
<cloud COLOR="#f0f0f0" SHAPE="ARC"/>
</node>
<node TEXT="" POSITION="right" ID="ID_1725864525" CREATED="1542479673780" MODIFIED="1542479741883">
<edge COLOR="#0000ff"/>
<hook NAME="SummaryNode"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="2.1)&#x41a;&#x430;&#x43a; &#x441;&#x43e;&#x437;&#x434;&#x430;&#x442;&#x44c; 3D &#x43c;&#x43e;&#x434;&#x435;&#x43b;&#x44c;?" ID="ID_1452444293" CREATED="1542479673792" MODIFIED="1542479741883" HGAP_QUANTITY="44.749999083578615 pt" VSHIFT_QUANTITY="-32.24999903887513 pt">
<font SIZE="20"/>
</node>
<node TEXT="" ID="ID_554582412" CREATED="1542479708369" MODIFIED="1542479740138">
<node TEXT="2.2)&#x420;&#x430;&#x437;&#x43d;&#x44b;&#x435; &#x430;&#x442;&#x43e;&#x43c;&#x44b;?" ID="ID_162640584" CREATED="1542479721235" MODIFIED="1542479740138" HGAP_QUANTITY="19.999999821186073 pt" VSHIFT_QUANTITY="29.999999105930357 pt">
<font SIZE="20"/>
</node>
</node>
</node>
<node TEXT="" POSITION="left" ID="ID_1432751756" CREATED="1542479362560" MODIFIED="1542479362564">
<edge COLOR="#00007c"/>
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="3) &#x418;&#x441;&#x441;&#x43b;&#x435;&#x434;&#x43e;&#x432;&#x430;&#x442;&#x435;&#x43b;&#x44c;&#x441;&#x43a;&#x430;&#x44f; &#x440;&#x430;&#x431;&#x43e;&#x442;&#x430; &#x43f;&#x43e; &#x438;&#x437;&#x43e;&#x43c;&#x435;&#x440;&#x430;&#x43c;" POSITION="left" ID="ID_240054941" CREATED="1542479205380" MODIFIED="1542479657596" HGAP_QUANTITY="84.49999789893633 pt" VSHIFT_QUANTITY="11.249999664723884 pt">
<edge COLOR="#0000ff"/>
<font SIZE="24"/>
<cloud COLOR="#f0f0f0" SHAPE="ARC"/>
</node>
<node TEXT="" POSITION="left" ID="ID_1589911424" CREATED="1542479362552" MODIFIED="1542479753797">
<edge COLOR="#7c0000"/>
<hook NAME="SummaryNode"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="3.1)&#x41e;&#x441;&#x43e;&#x431;&#x435;&#x43d;&#x43d;&#x43e;&#x441;&#x442;&#x438;?" ID="ID_334917495" CREATED="1542479362568" MODIFIED="1542479751995" HGAP_QUANTITY="79.99999803304678 pt" VSHIFT_QUANTITY="-81.74999756366022 pt">
<font SIZE="20"/>
</node>
<node TEXT="3.2)&#x41a;&#x430;&#x43a;&#x438;&#x435; &#x431;&#x44b;&#x432;&#x430;&#x44e;&#x442; &#x438;&#x437;&#x43e;&#x43c;&#x435;&#x440;&#x44b;? &#x41a;&#x430;&#x43a; &#x438;&#x445; &#x440;&#x430;&#x437;&#x43b;&#x438;&#x447;&#x430;&#x442;&#x44c;?" ID="ID_583041020" CREATED="1542479478320" MODIFIED="1542479753797" HGAP_QUANTITY="49.249998949468164 pt" VSHIFT_QUANTITY="33.74999899417165 pt">
<font SIZE="20"/>
</node>
</node>
</node>
</map>
